﻿using System.Data.Entity;

namespace ew_emails_console.Db
{
    public class EasyWeddingsDbContext : DbContext
    {
        public EasyWeddingsDbContext()
            : this("EasyWeddingsDbContext") { }

        public EasyWeddingsDbContext(string connectionString)
            : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public IDbSet<UserEnquiry> UserEnquiries { get; set; }
        public IDbSet<EnquiryResend> EnquiryResends { get; set; }
    }
}
