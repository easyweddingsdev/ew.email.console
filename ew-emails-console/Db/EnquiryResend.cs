﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ew_emails_console.Db
{
    [Table("temp_EnquiryResends")]
    public class EnquiryResend
    {
        [Key]
        public int ID { get; set; }
        public long EnquiryId { get; set; }
        public DateTime? ResendDate { get; set; }
    }
}
