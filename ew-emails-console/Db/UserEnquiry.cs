﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ew_emails_console.Db
{
    [Table("usr_enquiry")]
    public class UserEnquiry
    {
        [Key]
        public long Id { get; set; }
        public string UserId { get; set; }
        public string UserEmail { get; set; }
        public long SupplierId { get; set; }
        public int CategoryId { get; set; }
        public int RegionId { get; set; }
        public DateTime? WeddingDate { get; set; }
        public string WeddingLocation { get; set; }
        public string ContactNumber { get; set; }
        public string RequestDetail { get; set; }
        public string UserAgent { get; set; }
        public string IPAddress { get; set; }
        public DateTime? Datetime { get; set; }
        public int? EnquiryTypeEnum { get; set; }
        public Guid CorrelationId { get; set; }

    }

}
