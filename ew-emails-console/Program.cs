﻿using ew_emails_console.Db;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace GmailQuickstart
{
    class Program
    {
        static string[] Scopes = { GmailService.Scope.GmailReadonly };
        static string ApplicationName = "ew-emails monitor";

        static void Main(string[] args)
        {
            Console.WriteLine("Enter Start date: (yyyy-mm-dd HH:mm:ss):");
            string startString = Console.ReadLine();
            Console.WriteLine("Enter end date: (yyyy-mm-dd HH:mm:ss):");
            string endString = Console.ReadLine();
            Console.WriteLine("Write to Resend table? (y/n) ");
            string writeToResendTable = Console.ReadLine();

            DateTime startDate;
            DateTime endDate;

            if (DateTime.TryParse(startString, out startDate) && DateTime.TryParse(endString, out endDate))
            {
                var list = GetEnquiryList(startDate, endDate, 10);

                UserCredential credential;

                using (var stream =
                    new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
                {
                    string credPath = "ew-emails-cred.json";

                    var clientSecrets = GoogleClientSecrets.Load(stream).Secrets;

                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        clientSecrets,
                        Scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    Console.WriteLine("Credential file saved to: " + credPath);
                }

                var service = new GmailService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = ApplicationName,
                });

                UsersResource.LabelsResource.ListRequest request = service.Users.Labels.List("me");

                var sentIds = new List<long>();
                var unsentIds = new List<long>();
                int counter = 1;

                foreach (var enquiry in list)
                {
                    Console.Write($"\r{counter} of {list.Count}");
                    bool foundEmail = WasEmailSent(service, enquiry.CorrelationId.ToString());

                    if (!foundEmail)
                    {
                        unsentIds.Add(enquiry.Id);
                    }
                    else
                    {
                        sentIds.Add(enquiry.Id);
                    }

                    counter++;
                }

                Console.WriteLine("Sent IDs:");
                sentIds.ForEach(Console.WriteLine);

                Console.WriteLine("Unsent IDs:");
                unsentIds.ForEach(Console.WriteLine);

                if (writeToResendTable.Equals("y", StringComparison.OrdinalIgnoreCase))
                {
                    using (var context = new EasyWeddingsDbContext())
                    {
                        foreach (var unsentId in unsentIds)
                        {
                            var resend = new EnquiryResend {EnquiryId = unsentId};
                            context.EnquiryResends.Add(resend);
                        }

                        context.SaveChanges();
                    }
                }

                Console.WriteLine("Press any key");
                Console.Read();
            }
            else
            {
                Console.WriteLine("Invalid start date and/or end date");
                Console.Read();
            }

        }

        private static bool WasEmailSent(GmailService service, string searchGuid)
        {
            bool bFoundEmail = false;
            List<Message> result = new List<Message>();
            UsersResource.MessagesResource.ListRequest request = service.Users.Messages.List("me");
            request.Q = $"label:supplier-enquiry  {searchGuid}";
            request.MaxResults = 1;
            ListMessagesResponse response;

            response = request.Execute();
            if (response.Messages != null)
            {
                bFoundEmail = true;
            }

            return bFoundEmail;
        }

        private static List<UserEnquiry> GetEnquiryList(DateTime startDate, DateTime endDate, int utcOffsetHours)
        {
            List<UserEnquiry> list;
            startDate = startDate.AddHours(-1 * utcOffsetHours);
            endDate = endDate.AddHours(-1 * utcOffsetHours);

            using (var context = new EasyWeddingsDbContext())
            {
                list = (from e in context.UserEnquiries
                    where e.Datetime >= startDate && e.Datetime <= endDate
                    select e).ToList();
            }

            return list;
        }

    }

}